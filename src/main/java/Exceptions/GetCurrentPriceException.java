package Exceptions;

public class GetCurrentPriceException extends Exception {
    /**
     * Konstruktur für die Exception wenn es Fehler gibt den Preis per API abzufragen.
     */
    public GetCurrentPriceException(String message) {
        super(message);
    }
}
