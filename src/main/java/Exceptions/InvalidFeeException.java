package Exceptions;

public class InvalidFeeException extends Exception {
    /**
     * Konstruktur für die Exception wenn eine ungültige Gebühr eingeben wird.
     */
    public InvalidFeeException() {
        super("Invalid Fee!");
    }
}
