package Exceptions;

public class InvalidAmountException extends Exception {
    /**
     * Konstruktur für die Exception wenn ein ungültiger Betrag eingeben wird.
     */
    public InvalidAmountException() {
        super("Invalid Amount < 0!");
    }
}
