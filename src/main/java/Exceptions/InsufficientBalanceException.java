package Exceptions;

public class InsufficientBalanceException extends Exception {
    /**
     * Konstruktur für die Exception wenn das Bankkonto leer wäre.
     */
    public InsufficientBalanceException() {
        super("Insufficient Account Balance");
    }
}
