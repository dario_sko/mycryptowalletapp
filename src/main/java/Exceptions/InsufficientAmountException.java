package Exceptions;

public class InsufficientAmountException extends Exception {
    /**
     * Konstruktur für die Exception wenn der Betrag auf dem Wallet nicht ausreicht.
     */
    public InsufficientAmountException() {
        super("Insufficient Amount of Crypto in Wallet!");
    }
}
