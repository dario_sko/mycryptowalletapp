module mywalletcryptoapp {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.net.http;
    requires javafx.base;

    exports domain;
    opens com.mycompany.sample;
    opens ui;

}