package com.mycompany.sample;

import Exceptions.RetrieveDataException;
import Exceptions.SaveDataException;
import domain.BankAccount;
import domain.Datastore;
import domain.WalletList;
import infrastruktur.CurrentCurrencyPrices;
import infrastruktur.FileDataStore;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import ui.GlobalContext;

import java.io.IOException;
import java.util.ResourceBundle;

public class WalletApp extends Application {

    //UI Parts

    private static Stage mainStage;
    public static final String GLOBAL_WALLET_LIST = "walletlist";
    public static final String GLOBAL_BANK_ACCOUNT = "bankaccount";
    public static final String GLOBAL_CURRENT_CURRENCY_PRICES = "currencyprices";
    public static String GLOBAL_SELECTED_WALLET = "selectedWallet";

    public static void switchScene(String fxmlFile, String resourceBundle) {
        try {
            Parent root = FXMLLoader.load(WalletApp.class.getResource(fxmlFile), ResourceBundle.getBundle(resourceBundle));
            Scene scene = new Scene(root);
            mainStage.setScene(scene);
            mainStage.show();
        } catch (Exception e) {
            WalletApp.showErrorDialog("Could not load new scene!");
            e.printStackTrace();
        }
    }

    public static void showErrorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("An exception occurred: " + message);
        alert.showAndWait();
    }

    //File-Handling-Parts

    private BankAccount loadBankAccountFromFile() throws RetrieveDataException {
        Datastore datastore = new FileDataStore();
        BankAccount bankAccount = datastore.retrieveBankAccount();
        System.out.println("Bankaccount loaded!");
        return bankAccount;
    }

    private WalletList loadWalletListFromFile() throws RetrieveDataException {
        Datastore datastore = new FileDataStore();
        WalletList walletList = datastore.retrieveWalletList();
        System.out.println("WalletList loaded");
        return walletList;
    }

    private void storeBankAccountToFile(BankAccount bankAccount) throws SaveDataException {
        Datastore datastore = new FileDataStore();
        datastore.saveBankAccount(bankAccount);
    }

    private void storeWalletListToFile(WalletList walletList) throws SaveDataException {
        Datastore datastore = new FileDataStore();
        datastore.saveWalletList(walletList);
    }

    @Override
    public void start(Stage stage) throws IOException {

        mainStage = stage;

        BankAccount bankAccount = new BankAccount();
        WalletList walletList = new WalletList();

        try {
            bankAccount = loadBankAccountFromFile();
        } catch (RetrieveDataException e) {
            WalletApp.showErrorDialog("Error on loading BankAccount data. Using new empty account!");
            e.printStackTrace();
        }

        try {
            walletList = loadWalletListFromFile();
        } catch (RetrieveDataException e) {
            WalletApp.showErrorDialog("Error on loading WalletList data. Using new empty WalletList!");
            e.printStackTrace();
        }

        //Fill GlobalContext

        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_WALLET_LIST, walletList);
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_BANK_ACCOUNT, bankAccount);
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES, new CurrentCurrencyPrices());

        WalletApp.switchScene("main.fxml", "com.mycompany.sample.main");

        mainStage.setOnCloseRequest(event -> event.consume());

       /* AnchorPane root = FXMLLoader.load(WalletApp.class.getResource("main.fxml"),
                ResourceBundle.getBundle("com.mycompany.sample.main"));

        Scene scene = new Scene(root, 640, 480);
        stage.setScene(scene);
        stage.show();*/
    }

    @Override
    public void stop() {
        WalletList walletList =
                (WalletList) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_WALLET_LIST);
        BankAccount bankAccount =
                (BankAccount) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_BANK_ACCOUNT);

        try {
            storeBankAccountToFile(bankAccount);
            System.out.println("BankAccount details stored to file!");
        } catch (SaveDataException e) {
            WalletApp.showErrorDialog("Could not store bankaccount details!");
            e.printStackTrace();
        }

        try {
            storeWalletListToFile(walletList);
            System.out.println("WalletList stored to file!");
        } catch (SaveDataException e) {
            WalletApp.showErrorDialog("Could not store walletList!");
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}

