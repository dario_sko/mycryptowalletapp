package domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class WalletList implements Serializable {

    private final HashMap<CryptoCurrency, Wallet> wallets;

    /**
     * Konstruktor erstellt neue HashMap
     */
    public WalletList() {
        this.wallets = new HashMap<>();
    }

    /**
     * Methode um eine Wallet in die Wallet-Liste hinzufügen.
     * Wird überprüft ob es Null ist und ob diese Wallet mit dem gleichen Namen schon existiert.
     *
     * @param wallet
     */
    public void addWallet(Wallet wallet) {
        if (wallet != null && !this.wallets.containsKey(wallet.getCryptoCurrency())) {
            this.wallets.put(wallet.getCryptoCurrency(), wallet);
        }
    }

    /**
     * Gibt genau die Wallet aus, die mit dem ENUM als Parameter, der Methode übergeben wird, aus der Wallet-Liste
     * zurück.
     *
     * @param cryptoCurrency
     * @return Wallet aus der Wallet-Liste
     */
    public Wallet getWallet(CryptoCurrency cryptoCurrency) {
        return this.wallets.get(cryptoCurrency);
    }

    /**
     * Gibt eine Liste aller Wallets zurück.
     *
     * @return Liste aller Wallets
     */
    public List<Wallet> getWalletsAsObservableList() {
        return wallets.values().stream().collect(Collectors.toList());
    }


    @Override
    public String toString() {
        return "WalletList{" +
                "wallets=" + wallets +
                '}';
    }
}
