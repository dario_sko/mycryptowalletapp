package domain;

import Exceptions.InsufficientBalanceException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class BankAccount implements Serializable {

    private BigDecimal balance;

    /**
     * Konstruktur der die Balance mit der Klasse BigDecimal zuweist diese aber davor mit der setScale Methode
     * kaufmännisch auf 2 Nachkommastellen rundet.
     */
    public BankAccount() {
        this.balance = new BigDecimal("0").setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Liefert die Balance vom BankAccount zurück.
     *
     * @return Balance vom BankAccount
     */
    public BigDecimal getBalance() {
        return this.balance;
    }

    /**
     * Wird verwendet um einen Betrag auf das Bankkonto einzuzahlen.
     *
     * @param amount
     */
    public void deposit(BigDecimal amount) {
        if (amount != null) {
            this.balance = this.balance.add(amount).setScale(2, RoundingMode.HALF_UP);
        }
    }

    /**
     * Wird verwendet um einen Betrag vom Bankkonto abzuheben.
     * Es wird überprüft, ob genug Geld vorhanden ist, falls ja, dann wird die neue Balance berechnet, wenn nicht,
     * dann wird eine Exception geworfen.
     *
     * @param amount
     * @throws InsufficientBalanceException
     */
    public void withdraw(BigDecimal amount) throws InsufficientBalanceException {
        if (amount != null) {
            if (this.balance.subtract(amount).doubleValue() >= 0) {
                this.balance = this.balance.subtract(amount).setScale(2, RoundingMode.HALF_UP);
            } else {
                throw new InsufficientBalanceException();
            }
        }
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}
